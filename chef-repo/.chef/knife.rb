# cookbook_path    ["cookbooks", "site-cookbooks"]
node_path        "nodes"
role_path        "roles"
environment_path "environments"
data_bag_path    "data_bags"
#encrypted_data_bag_secret "data_bag_key"

knife[:berkshelf_path] = "cookbooks"

node_name		'chef-server'
client_key               '/var/mychef/chef-repo/.chef/client.pem'
cookbook_path            [ '/var/mychef/chef-repo/cookbooks','/var/mychef/chef-repo/site-cookbooks'  ]
local_mode true


