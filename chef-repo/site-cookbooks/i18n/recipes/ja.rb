# i18n ja

bash "ja" do
  user 'root'
  code <<-EOC
    sed -i -e "s/LANG=\".*\"/LANG=\"ja_JP.UTF-8\"/" /etc/sysconfig/i18n
    # . /etc/sysconfig/i18n
  EOC
end

