# sshd settings
service "sshd" do
  supports :status => true, :restart => true
  action [ :enable, :start ]
end

template "/etc/ssh/sshd_config" do
  source "sshd_config.erb"
  group "root"
  owner "root"
  mode "400"
  notifies :restart, "service[sshd]"
end

