# template iptables
bash "iptables template" do
  user 'root'
  code <<-EOC

# フィルタリングルールを消去する
/sbin/iptables -F

# デフォルトポリシー設定
/sbin/iptables -P INPUT DROP
/sbin/iptables -P FORWARD DROP
/sbin/iptables -P OUTPUT DROP

# ループバックを許可する

/sbin/iptables -A INPUT -i lo -j ACCEPT
/sbin/iptables -A OUTPUT -o lo -j ACCEPT

# プライベートアドレスが使われているパケットを破棄
#/sbin/iptables -A INPUT -i eth0 -s 10.0.0.0/8 -j DROP
#/sbin/iptables -A INPUT -i eth0 -d 10.0.0.0/8 -j DROP
#/sbin/iptables -A INPUT -i eth0 -s 172.16.0.0/12 -j DROP
#/sbin/iptables -A INPUT -i eth0 -d 172.16.0.0/12 -j DROP
#/sbin/iptables -A INPUT -i eth0 -s 192.168.0.0/16 -j DROP
#/sbin/iptables -A INPUT -i eth0 -d 192.168.0.0/16 -j DROP

#ssh
#/sbin/iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT
/sbin/iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 59410 -j ACCEPT


#http
/sbin/iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
/sbin/iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 443 -j ACCEPT
#/sbin/iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 941 -j ACCEPT

#samba
#/sbin/iptables -A INPUT -m state --state NEW  -p udp --dport 127:138 -j ACCEPT
#/sbin/iptables -A INPUT -m state --state NEW  -p tcp --dport 139 -j ACCEPT


# Ping of Death対策
/sbin/iptables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT
/sbin/iptables -A INPUT -p icmp --icmp-type echo-request -j DROP


#確立しているものは許可
/sbin/iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# 外への接続は全て許可
/sbin/iptables -P OUTPUT ACCEPT

# 保存
/etc/init.d/iptables save

# 再起動
/etc/init.d/iptables restart

# 確認
/sbin/iptables -L -v

  EOC
end
