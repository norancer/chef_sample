# disable iptables

bash "iptables disabled" do 
  user 'root'
  code <<-EOC
    service iptables stop
    chkconfig iptables off
  EOC
end

