# timezone Asia/Tokyo

bash "timezone Asia/Tokyo" do
  user 'root'
  code <<-EOC
    echo y | cp -pf  /usr/share/zoneinfo/Japan /etc/localtime
  EOC
end

