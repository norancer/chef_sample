# disable firewalld

bash "firewalld disabled" do 
  user 'root'
  code <<-EOC
    systemctl stop firewalld
    systemctl disable firewalld
  EOC
end

