# install ntpd
package "ntp" do
  action :install
end

service "ntpd" do
  supports :status => true, :restart => true
  action [ :enable, :start ]
end

template "/etc/ntp.conf" do
  source "ntp.conf"
  group "root"
  owner "root"
  mode "400"
  notifies :restart, "service[ntpd]"
end

